package jp.alhinc.nosaki_kenshi.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CalculateSales {
	public static void main (String[] args) {

		BufferedReader br1 = null;
		BufferedReader br2 = null;

		Map<String, String> branchMap =  new HashMap<>();
		Map<String, Long> salesMap =  new HashMap<>();
		List<Integer> salesList = new ArrayList<>();

		try {
//			支店ファイル
			try {
				File file1 = new File(args[0], "branch.lst");
				if(!file1.exists()) {
					System.out.println("支店定義ファイルが存在しません");
					return;
				}
				FileReader fr1 = new FileReader(file1);
				br1 = new BufferedReader(fr1);

				String line1;
				while((line1 = br1.readLine()) != null) {
					if(!line1.matches("[0-9]{3},(?!.*,).*")) {
						System.out.println("支店定義ファイルのフォーマットが不正です");
						return;
					}
					String[] lines = line1.split(",");
					branchMap.put(lines[0], lines[1]);
					salesMap.put(lines[0], 0L);
				}
			} finally {
				if(br1 != null) {
					br1.close();
				}
			}

//			売上ファイル
			File file2 = new File(args[0]);
			String[] fileName = file2.list();

			for(int i = 0; i < fileName.length; ++i) {
				int point = fileName[i].lastIndexOf(".");
				String extension = fileName[i].substring(point + 1);
				String woext = fileName[i].substring(0, point);
				if(extension.equals("rcd") && woext.matches("[0-9]{8}")){
					salesList.add(Integer.parseInt(woext));

					try {
						File file3 = new File(args[0], fileName[i]);
						br2 = new BufferedReader(new FileReader(file3));

						String x = "";
						long y = 0;
						x = br2.readLine();
						y = Long.parseLong(br2.readLine());
						if(br2.readLine() != null) {
							System.out.println(fileName[i] + "のフォーマットが不正です");
							return;
						}
						if(salesMap.get(x) == null) {
							salesMap.put(x, y);
						} else {
							salesMap.put(x, salesMap.get(x) + y);
							if(String.valueOf(salesMap.get(x)).length() > 10) {
								System.out.println("合計金額が10桁を超えました");
								return;
							}
						}
						if(!branchMap.containsKey(x)) {
							System.out.println(fileName[i] + "の支店コードが不正です");
							return;
						}
					} finally{
						if(br2 != null) {
							br2.close();
						}
					}
				}
			}
			Collections.sort(salesList);
			if(salesList.size() != salesList.get(salesList.size()-1)-salesList.get(0)+1) {
				System.out.println("売上ファイル名が連番になっていません");
				return;
			}


//			集計結果出力
			File file3 = new File(args[0],"branch.out");
			FileWriter fw = new FileWriter(file3);
			BufferedWriter bw = new BufferedWriter(fw);

			for(String key : salesMap.keySet()) {
				bw.write(key + "," + branchMap.get(key) +  "," + salesMap.get(key) + "\n");
			}
			bw.close();

		} catch(IOException e) {
			System.out.println("予期せぬエラーが発生しました。");
		}
	}
}
